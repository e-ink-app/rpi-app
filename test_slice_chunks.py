# -*- coding: utf-8 -*-

import slice_chunks as slicer

text = '''
Snijd de onderkant van de stronken witlof. Halveer het lof in de lengte, verwijder de harde kern en snijd de blaadjes in repen van 1 cm.
±Kook de spaghetti volgens de aanwijzingen op de verpakking beetgaar.±Snijd ondertussen de ham in blokjes. Verhit de olie in een koekenpan en bak de ham 2 min. Voeg het witlof toe en bak 2 min. Schep regelmatig om.
±Schenk de slagroom erbij en laat op laag vuur 4 min. zachtjes koken. Breng op smaak met peper en eventueel zout. Schep het witlofmengsel door de spaghetti. Rasp de kaas erboven en serveer direct.
'''

def test_createStringArray():
  pass


def test_chunksplit():
  pre = '''Snijd@Kook de '''
  expected = ['Snijd', 'Kook de ']
  splits = slicer.split_chunk(pre)
  result = []
  result.append(splits[0])
  print(result)
  assert all([a == b for a, b in zip(result, [expected[0]])])