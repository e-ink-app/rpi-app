import sys
import os
import logging
import epdconfig
import epd7in5
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import firebase_admin
from firebase_admin import credentials, firestore

cred = credentials.Certificate("e-ink-app-d9474-firebase-adminsdk-mw0xc-3b1be069e5.json")
default_app = firebase_admin.initialize_app(cred)
db = firestore.client()

test_ref = db.collection(u'common').document(u'text')
string_array = []
current_raw_string = ""

def chunks(s, n):
    """Produce `n`-character chunks from `s`."""
    for start in range(0, len(s), n):
        yield s[start:start+n]

def split_chunk(chunk):
  return chunk.split('@')

def flatten(A):
    rt = []
    for i in A:
        if isinstance(i,list): rt.extend(flatten(i))
        else: rt.append(i)
    return rt

def createParagraphs(string):
   return split_chunk(string)

def formatParagraphs(input_arr):
  string_array = []
  for item in input_arr:
      item_arr = []
      for chunk in chunks(item, 42):
        item_arr.append(chunk + u'             .')
      string_array.append(item_arr)
      string_array.append(u"@")
  return string_array

def writeTextToScreen(raw_data):
  paragraphed = createParagraphs(raw_data)
  paragraphed_arr = formatParagraphs(paragraphed)
  paragraphed_arr_flat = flatten(paragraphed_arr)

  logging.basicConfig(level=logging.DEBUG)

  try:
    logging.info('Test script!')
    current_height = 0

    epd = epd7in5.EPD()
    logging.info('Initting everything and clearing the screen')
    epd.init()
    epd.Clear()

    font24 = ImageFont.truetype('Font.ttc', 24)
    font18 = ImageFont.truetype('Font.ttc', 18)
    logging.info('Setting big amount of text')

    Himage = Image.new('1', (epd.height, epd.width), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(Himage)
    for string in paragraphed_arr_flat:
      if string == "@":
        current_height += 18

      else:
        draw.text((10, current_height), string, font = font18, fill = 0)

      current_height += 18
    
    epd.display(epd.getbuffer(Himage))

  except IOError as e:
    logging.info('Something went wrong...')
    logging.info(e)

  except KeyboardInterrupt:
    logging.info("ctrl + c:")
    epd7in5.epdconfig.module_exit()
    exit()

while True:
  raw_data = test_ref.get().get(u'value')
  # print(raw_data)
  if raw_data == current_raw_string:
    print('Input is the same.. Not updating.')
  else:
    current_raw_string = raw_data
    writeTextToScreen(raw_data)

  print('going to sleep....')
  time.sleep(300)
