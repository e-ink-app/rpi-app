import sys
import os
import logging
import epdconfig
import epd7in5
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import firebase_admin
from firebase_admin import credentials, firestore

cred = credentials.Certificate("e-ink-app-d9474-firebase-adminsdk-mw0xc-3b1be069e5.json")
default_app = firebase_admin.initialize_app(cred)
db = firestore.client()

test_ref = db.collection(u'common').document(u'text')

raw_data = test_ref.get().get(u'value')
string_array = []

def chunks(s, n):
    """Produce `n`-character chunks from `s`."""
    for start in range(0, len(s), n):
        yield s[start:start+n]

for chunk in chunks(raw_data, 42):
    if "/n" in chunk:
        formatted_chunk = chunk.replace('/n', '')
        string_array.append(formatted_chunk)
        string_array.append('~~')
    else:
      string_array.append(chunk + '        .')

last_line = string_array[len(string_array) - 1]
last_line = last_line.rstrip('.')
string_array.pop()
string_array.append(last_line)

logging.basicConfig(level=logging.DEBUG)

try:
  logging.info('Test script!')
  current_height = 0

  epd = epd7in5.EPD()
  logging.info('Initting everything and clearing the screen')
  epd.init()
  epd.Clear()

  font24 = ImageFont.truetype('Font.ttc', 24)
  font18 = ImageFont.truetype('Font.ttc', 18)
  logging.info('Setting big amount of text')

  Himage = Image.new('1', (epd.height, epd.width), 255)  # 255: clear the frame
  draw = ImageDraw.Draw(Himage)
  for string in string_array:
    if string == "~~":
      current_height += 18

    else:
      draw.text((10, current_height), string, font = font18, fill = 0)

    current_height += 18
  
  epd.display(epd.getbuffer(Himage))

except IOError as e:
  logging.info('Something went wrong...')
  logging.info(e)

except KeyboardInterrupt:
  logging.info("ctrl + c:")
  epd7in5.epdconfig.module_exit()
  exit()