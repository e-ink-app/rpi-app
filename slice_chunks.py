# -*- coding: utf-8 -*-

string = u'Snijd de onderkant van de stronken witlof. Halveer het lof in de lengte, verwijder de harde kern en snijd de blaadjes in repen van 1 cm.@Kook de spaghetti volgens de aanwijzingen op de verpakking beetgaar.@Snijd ondertussen de ham in blokjes. Verhit de olie in een koekenpan en bak de ham 2 min. Voeg het witlof toe en bak 2 min. Schep regelmatig om.@Schenk de slagroom erbij en laat op laag vuur 4 min. zachtjes koken. Breng op smaak met peper en eventueel zout. Schep het witlofmengsel door de spaghetti. Rasp de kaas erboven en serveer direct. '

def chunks(s, n):
    """Produce `n`-character chunks from `s`."""
    for start in range(0, len(s), n):
        yield s[start:start+n]

def split_chunk(chunk):
  return chunk.split('@')

def flatten(A):
    rt = []
    for i in A:
        if isinstance(i,list): rt.extend(flatten(i))
        else: rt.append(i)
    return rt

def createParagraphs(string):
   return split_chunk(string)

def formatParagraphs(input_arr):
  string_array = []
  for item in input_arr:
      item_arr = []
      for chunk in chunks(item, 42):
        item_arr.append(chunk + u'        .')
      string_array.append(item_arr)
      string_array.append(u"@")
  return string_array

paragraphed = createParagraphs(string)
paragraphed_arr = formatParagraphs(paragraphed)
paragraphed_arr_flat = flatten(paragraphed_arr)

# print(string_array)
# formatted_array = flatten(string_array)
# kek = string_array[len(string_array) - 1]
# kek = kek.rstrip('.')
# string_array.pop()
# string_array.append(kek)

# print(formatted_array)
for line in paragraphed_arr_flat:
    print(line)

def createParagraphArray_2(string):
  string_array = []
  for chunk in chunks(string, 42):
      if u"@" in chunk:
          formatted_chunk = split_chunk(chunk)
          formatted_chunk.insert(1, '@')
          string_array.append(formatted_chunk)
      else:
          string_array.append(chunk + u'        .')
  return string_array
